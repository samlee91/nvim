#! /usr/bin/env zsh

# language servers
sudo pacman -S --needed --noconfirm \
bash-language-server \
ccls \
rust-analyzer \
pyright \
lua-language-server \
tree-sitter-cli \
texlab \

yay -S --needed --noconfirm \
cssmodules-language-server \
