--> zsh

vim.api.nvim_create_autocmd({ "BufEnter", "BufRead", "BufNewFile" }, {
   group = vim.api.nvim_create_augroup("ZshNewFile", { clear=true }),
   callback = function(bufnr)
      vim.treesitter.start(0, "bash")
   end
})
