--> rmd

vim.wo.breakindent = true
vim.wo.lbr = true
vim.wo.wrap = true
vim.treesitter.start(0, "latex")

vim.keymap.set("n", "j", "gj", { buffer=true })
vim.keymap.set("n", "k", "gk", { buffer=true })
vim.keymap.set("n", "<leader>rr", ":!Rscript -e \"rmarkdown::render('%')\" <Return>", { silent=true })
vim.keymap.set("i", "CC", "<!--  --><Esc>hhhi")
