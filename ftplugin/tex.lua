--> LaTeX

LaTeXCompileOnWrite = true
LaTeXCleanBuildFiles = true

--> formatting
vim.wo.breakindent = true
vim.wo.lbr = true
vim.wo.wrap = true

--> spell check
vim.opt_local.spell = true
vim.opt_local.spellcapcheck = ""
vim.opt_local.spellsuggest = "best,5"
vim.opt_local.spelloptions = { "camel" } --> ignore CamelCase words

vim.keymap.set("n", "j", "gj", { buffer=true })

vim.keymap.set("n", "j", "gj", { buffer=true })
vim.keymap.set("n", "k", "gk", { buffer=true })
vim.keymap.set("n", "<leader>cl", "<cmd>!pdflatex '%' <Return>", { silent=true })


--> compile on write and remove build files
vim.api.nvim_create_autocmd({"BufWritePost", "FileWritePost"}, {
   group = vim.api.nvim_create_augroup("LaTeXCompile", { clear=true }),

   callback = function()
      if LaTeXCompileOnWrite then
         local latex_dir = vim.fn.expand("%:h")
         local latex_filename = vim.fn.expand("%:r")
         local latex_extension = vim.fn.expand("%:e")

         if latex_extension == "tex" then
            vim.cmd(string.format(
               "!pdflatex -cnf-line='TEX_HUSH=all' -interaction=batchmode -output-directory=%s %s.tex",
               latex_dir,
               latex_filename
            ))

            if LaTeXCleanBuildFiles then
               vim.cmd(string.format(
                  "!rm %s.log %s.aux",
                  latex_filename,
                  latex_filename
               ))
            end
         end
      end
   end
})
