--> scss

vim.keymap.set("n", "<leader>cc", "I/* <Esc>A */<Esc>%")
vim.keymap.set("n", "<leader>cu", ":s; *\\*\\/ *;;g <Return> :s;\\/\\* *;;g <Return>")
