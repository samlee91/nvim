
--> comment
vim.keymap.set( "n", "<leader>cc", "I/* <Esc>A */<Esc>%" )
vim.keymap.set( "v", "<leader>cc", "c/*<Return>*/<Esc>kp" )
vim.keymap.set( "i", "CC", "/*  */<Esc><left><left>i" )

--> uncomment
vim.keymap.set( "n", "<leader>cu", function()
   local current_line = vim.api.nvim_win_get_cursor(0)[1]
   local current_buf = vim.api.nvim_get_current_buf()
   local comment_start = vim.fn.searchpos( "\\/\\*", "n" )
   local comment_end = vim.fn.searchpos( "\\*\\/", "n" )

   if comment_start[1] == current_line and
      comment_end[1] >= comment_start[1] then

      vim.api.nvim_buf_set_mark( current_buf, "a", comment_start[1], comment_start[2]-1, {} )
      vim.api.nvim_buf_set_mark( current_buf, "b", comment_end[1], comment_end[2], {} )

      vim.api.nvim_win_set_cursor( 0, comment_start )
      local text_start = vim.fn.searchpos( "[^ \\*]", "n" )
      vim.api.nvim_buf_set_mark( current_buf, "A", text_start[1], text_start[2]-1, {} )

      vim.api.nvim_win_set_cursor( 0, comment_end )
      local text_end = vim.fn.searchpos( "[^ \\*]", "bn" )
      vim.api.nvim_buf_set_mark( current_buf, "B", text_end[1], text_end[2]-1, {} )

      vim.api.nvim_feedkeys( "`Av`Bd`bv`aP", "n", true )
      return
   end

   vim.print("invalid comment region")

end)

