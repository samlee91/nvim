--> css

--> CommentChars = {
-->    ["lua"] = "--> ",
-->    ["c"] = "//",
--> }

vim.keymap.set({ "n", "v" }, "<leader>cc", "I/* <Esc>A */<Esc>%")
vim.keymap.set("n", "<leader>cu", ":s; *\\*\\/ *;;g <Return> :s;\\/\\* *;;g <Return>")

--> CommentChar = CommentChars["lua"]
