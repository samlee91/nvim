require("global")
require("theme")
require("statusline")
require("keymaps")
require("plugins")

--> TODO:
--> adjust terminal margins at launch to fix tabline/statusline padding
--> os.execute("kitten @ set-spacing padding=0 && kill -s SIGUSR1 56082")
