require("statusline.statusline")


vim.o.showmode = false
vim.o.laststatus = 3
vim.o.statusline = "%!luaeval('build_statusline()')"

