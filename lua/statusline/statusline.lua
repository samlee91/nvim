
local function slstring(str, color)
   return string.format( "%%#%s#%s", color, str )
end


local padding = slstring(" ", "StatusLineTab")
local right_separator = slstring("", "StatusLineRightSeparator")
local left_separator  = slstring("", "StatusLineLeftSeparator")
local outside_left_separator  = slstring("", "StatusLineOutsideLeftSeparator")
local outside_right_separator = slstring("", "StatusLineOutsideRightSeparator")


local mode_icons = {
   ["n"]   = slstring( " ", "@statusline.mode.icon.normal" ),
   ["i"]   = slstring( " ", "@statusline.mode.icon.insert" ),
   ["c"]   = slstring( " ", "@statusline.mode.icon.command" ),
   ["v"]   = slstring( " ", "@statusline.mode.icon.visual" ),
   ["V"]   = slstring( " ", "@statusline.mode.icon.visual_line")
          .. slstring("  ", "@statusline.mode.text" ),
   ["\22"] = slstring( " ", "@statusline.mode.icon.visual_block" )
          .. slstring("  ", "@statusline.mode.text" ),
}

local percent_icons = {
   " ", " ", " ",
   " ", " ", " ",
   " ", " ", " ",
   " ", " ", " ",
   " ", " ", " "
}

local percent_pair_icons = {
   {"", ""}, {"", ""}, {"", ""}, {"", ""},
   {"", ""}, {"", ""}, {"", ""}, {"", ""},
   {"", ""}, {"", ""}, {"", ""}, {"", ""},
   {"", ""},
}


local function get_mode_icons( iconset )
   local mode_icon = iconset[vim.fn.mode()]

   if mode_icon ~= nil then
      return mode_icon
   end
end


local function get_cursor_position_icons()
   return slstring("▶[", "@statusline.cursor.icon")
       .. slstring("%l,%c", "@statusline.cursor.text")
       .. slstring("]", "@statusline.cursor.icon")
end


local function get_buffer_percent_icons()
   return slstring( " ", "@statusline.percent.icon")
      ..  slstring( "%p%%", "@statusline.percent.text" )
end


local function percent_module()
   local buf_percent = vim.api.nvim_win_get_cursor(0)[1] / vim.fn.line("$")
   local icon_idx = math.ceil( buf_percent * #percent_icons )
   local icon = percent_icons[ icon_idx ]

   return slstring( icon, "@statusline.percent.icon" )
end


local function percent_pair_module()
   local buf_percent = vim.api.nvim_win_get_cursor(0)[1] / vim.fn.line("$")
   local icon_idx = math.ceil( buf_percent * #percent_pair_icons )
   local icons = percent_pair_icons[ icon_idx ]

   return slstring( icons[1], "@statusline.percent.icon.1" )
       .. slstring( icons[2], "@statusline.percent.icon.2" )
end


local function get_file_size()
   local units = { "B", "kB", "MB", "GB", "TB" }
   local file_size = tonumber( vim.inspect(vim.fn.wordcount()["bytes"]) )
   local decimal_places = 2
   local i = 1

   while file_size >= 1000 do
      file_size = file_size / 1000
      i = i + 1
   end

   local rounded_size = math.floor(( 10^decimal_places * file_size + 0.5 )) / 10^decimal_places
   return slstring("猪", "@statusline.filesize.icon")
       .. slstring(rounded_size, "@statusline.filesize.text" )
       .. slstring(units[i], "@statusline.filesize.icon")
end



local function wrap( str )
   return string.format( "%s%s%s%s%s", left_separator, padding, str, padding, right_separator )
end



function build_statusline()
   return "%=" .. outside_left_separator
               .. wrap( get_mode_icons( mode_icons ) )
               .. wrap( get_cursor_position_icons() )
               .. wrap( get_buffer_percent_icons() )
               .. outside_right_separator
               .. "%="
end
