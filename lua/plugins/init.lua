--> lazy bootstrap
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
   vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      "https://github.com/folke/lazy.nvim.git",
      "--branch=stable",
      lazypath,
   })
end

vim.opt.rtp:prepend(lazypath)


--> lazy setup
local status, lazy = pcall(require, "lazy")
if not status then
   print("lazy bootstrap failed.")
   return
end


local lazy_settings = {
   defaults = {
      lazy = true,
   },

   ui = {
      border = "rounded",
      wrap = true,
      pills = true,
      zindex = 2,
      title = "",
      title_pos = "left",
      size = { width=0.5, height=0.6 },

      icons = {
         cmd = " ",
         config = " ",
         event = " ",
         ft = " ",
         init = " ",
         import = " ",
         keys = " ",
         lazy = " ",
         loaded = "●",
         not_loaded = "○",
         plugin = " ",
         runtime = " ",
         source = " ",
         start = "",
         task = "✔ ",
         list = { "●", "➜", "★", "‒", },
      },
   },

   lockfile = vim.fn.stdpath("config") .."/lua/plugins/lazy-lock.json",

   checker = {
      notify = false,
   }
}


lazy.setup("plugins.config", lazy_settings)
