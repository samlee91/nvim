
return {
   "neovim/nvim-lspconfig",
   enabled = true,
   lazy = false,

   config = function()
      local lsp_status, lspconfig = pcall(require, "lspconfig")
      if not lsp_status then
         return
      end

      --> Lspinfo popups
      local ui_status, windows = pcall(require, "lspconfig.ui.windows")
      if ui_status then

         windows.default_options = {
            border = "rounded",
            winhighlight = "Normal:@popup.cmp,CursorLine:@popup.select.cmp,FloatBorder:@popup.border.dark",
         }
      end

      --> floating diagnostics window
      vim.diagnostic.config({
         virtual_text = false,
         float = {
            border = "rounded",
            max_width = 50,
            max_height = 60,
            zindex = 2,
         },
      })

      local signs = {
         Error = "▶",
         Warn  = "▶",
         Hint  = "▶",
         Info  = "▶",
      }

      for key, value in pairs(signs) do
          local hl = "DiagnosticSign" .. key
          vim.fn.sign_define(hl, { text=value, texthl=hl, numhl=hl })
      end


      vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
         vim.lsp.handlers.hover, {
            border = "rounded",
            max_width = 70,
            max_height = 70,
            zindex = 1,
      })

      --> vim.lsp.handlers["textDocument/definition"] = vim.lsp.with(
      -->    vim.lsp.handlers.hover, {
      -->       border = "rounded",
      -->       max_width = 70,
      -->       max_height = 70,
      -->       zindex = 1,
      --> })

      --> language servers
      local lsp_capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

      local function lsp_on_attach(client, bufnr)
         client.server_capabilities.semanticTokensProvider = nil
      end


      --> c
      lspconfig["ccls"].setup({
         capabilities = lsp_capabilities,
         on_attach = lsp_on_attach,
      })

      --> rust
      lspconfig["rust_analyzer"].setup({
         capabilities = lsp_capabilities,
         on_attach = lsp_on_attach,
      })

      --> LaTeX
      lspconfig["texlab"].setup({
         capabilities = lsp_capabilities,
         on_attach = lsp_on_attach,
      })

      --> css
      lspconfig["cssls"].setup({
         capabilities = lsp_capabilities,
         on_attach = lsp_on_attach,
      })

      lspconfig["julials"].setup({
         capabilities = lsp_capabilities,
         on_attach = lsp_on_attach,

         on_new_config = function(new_config, _)
            local julia = vim.fn.expand("~/.local/share/julia/environments/nvim-lspconfig/bin/julia")
            if require'lspconfig'.util.path.is_file(julia) then
               new_config.cmd[1] = julia
            end
         end,

         root_dir = function(fname)
             local util = require'lspconfig.util'
             return util.root_pattern 'Project.toml'(fname) or util.find_git_ancestor(fname) or
                    util.path.dirname(fname)
         end,
      })

      --> lua
      lspconfig["lua_ls"].setup({
         capabilities = lsp_capabilities,
         on_attach = lsp_on_attach,
         log_level = 3,
         settings = {
            Lua = {
               elemetry = { enabled=false },
                    runtime   = { version="LuaJIT" },
                    workspace = {
                       checkThirdParty = false,
                       library = { vim.env.VIMRUNTIME }
                    },
                     diagnostics = {
                        trailing_space = false,
                        globals = { "vim", "bufnr" },
                     },
                  }
               },

         on_init = function(client)
            local path = client.workspace_folders[1].name

            if not vim.loop.fs_stat(path.."/.luarc.json") and not vim.loop.fs_stat(path.."/.luarc.jsonc") then
               client.config.settings = vim.tbl_deep_extend("force", client.config.settings, {
                  Lua = {
                     telemetry = { enabled=false },
                     runtime   = { version="LuaJIT" },
                     workspace = {
                        checkThirdParty = false,
                        library = { vim.env.VIMRUNTIME }
                     },
                     diagnostics = {
                        trailing_space = false,
                        globals = { "vim", "bufnr" },
                     },
                  }
               })

               client.notify("workspace/didChangeConfiguration", { settings=client.config.settings })
            end

            return true
         end
      })

      --> python
      lspconfig["pyright"].setup({
         capabilities = lsp_capabilities,
         on_attach = lsp_on_attach,
         settings = {
            python = {
               analysis = {
                  diagnosticSeverityOverrides = {
                     reportMissingImports = "none",
                     reportMissingModuleSource = "none",
                  },
               },
            },
         },
      })

      lspconfig["bashls"].setup({
         capabilities = lsp_capabilities,
         on_attach = lsp_on_attach,
         filetypes = { "sh", "bash", "zsh" },
      })
   end
}
