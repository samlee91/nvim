
return {
   "echasnovski/mini.animate",
   enabled = true,
   lazy = false,
   version = "*",

   config = function()
      local status, animate  = pcall(require, "mini.animate")
      if not status then
         return
      end

      local function is_many_wins(sizes_from, sizes_to)
         return vim.tbl_count(sizes_from) >= 3
      end

      local function is_not_single_window(win_id)
         return #vim.api.nvim_tabpage_list_wins(
            vim.api.nvim_win_get_tabpage(win_id)
         ) > 1
      end

      local function default_predicate()
         return true
      end

      local default_timing = animate.gen_timing.linear({ duration=80, unit="total" })
      local default_line = animate.gen_path.line({ predicate=default_predicate })

      animate.setup({
         cursor = {
            enable = true,
            timing = default_timing,
            path = default_line,
         },

         --> Vertical scroll
         scroll = {
            enable = true,
            timing = default_timing,
            subscroll = animate.gen_subscroll.equal({
               max_output_steps = 120,
               predicate = default_predicate,
            }),
         },

         --> Window resize
         resize = {
            enable = true,
            timing = animate.gen_timing.linear({
               duration = 250,
               unit = "total",
            }),
            subresize = animate.gen_subresize.equal({ predicate=is_many_wins }),
         },

         --> Window open
         open = {
            enable = false,
            timing = animate.gen_timing.linear({ duration=250, unit="total" }),
            winconfig = animate.gen_winconfig.wipe({
               predicate = is_not_single_window,
               direction = "from_edge",
            }),
            winblend = animate.gen_winblend.linear({ from=80, to=100 }),
         },

         --> Window close
         close = {
            enable = false,
            timing = animate.gen_timing.linear({ duration=250, unit="total" }),
            winconfig = animate.gen_winconfig.wipe({
               predicate = is_not_single_window,
               direction = "to_edge",
            }),
            winblend = animate.gen_winblend.linear({ from=100, to=80 }),
         },
      })
   end

}
