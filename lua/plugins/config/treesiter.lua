
return {
   "nvim-treesitter/nvim-treesitter",
   lazy = false,
   enabled = true,

   build = { ":TSUpdate" },
   dependencies = { "nvim-treesitter/playground" },

   config = function()
      local status, treesitter = pcall(require, "nvim-treesitter.configs")
      if not status then
         return
      end

      treesitter.setup({
         sync_install = false,
         auto_install = true,

         ensure_installed = { "bash", "c", "cpp", "lua", "markdown", "markdown_inline", "regex", "rust", "python", "julia", "yaml" },

         highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
            disable = function(bufnr)
               local filetype = vim.bo.filetype
               return filetype == "rmd"
            end
         },

         indent = {
            enable = true,
         },

         playground = {
            enable = true
         }
      })

      vim.keymap.set("n", "<leader>tt", ":TSPlaygroundToggle <Return>")
      vim.keymap.set("n", "<leader>hg", ":TSHighlightCapturesUnderCursor <Return>", { silent=true })
   end
}

