return {
   "nvim-tree/nvim-web-devicons",
   --> tag = { "nerd-v2-compat" },
   enabled = true,

   config = function()
      local status, devicons = pcall(require, "nvim-web-devicons")
      if not status then
         return
      end

      devicons.set_default_icon( "", Foreground )

      devicons.setup({
         color_icons = true,
         default = true,

         override = {
            c        = { icon="",  color=Blue,   name="C" },
            bash     = { icon="",  color=Blue,   name="Bash" },
            sh       = { icon="",  color=Green,  name="Sh" },
            cpp      = { icon="",  color=Purple, name="Cpp" },
            py       = { icon="",  color=Yellow, name="Python" },
            r        = { icon="ﳒ",  color=Green,  name="R" },
            zsh      = { icon="",  color=Green,  name="Zsh" },
            txt      = { icon="",  color=White,  name="Text" },
            tex      = { icon="",  color=Red,    name="LaTeX" },
            md       = { icon="",  color=Purple, name="Markdown" },
            help     = { icon="",  color=Yellow, name="Help" },
            html     = { icon="",  color=Red,    name="Html" },
            hyprlang = { icon=" ", color=Cyan,   name="Hyprlang" },
            man      = { icon="",  color=Purple, name="Man" },
            netrw    = { icon="",  color=Blue,   name="Netrw" },
            lisp     = { icon="",  color=Purple, name="Lisp" },
            yuck     = { icon="",  color=Yellow, name="Yuck" },
            yml      = { icon="",  color=Orange, name="Yaml" },
         },

         override_by_filename = {
            [".zshrc"]         = { icon="", color=Green,  name="Zshrc" },
            [".bashrc"]        = { icon="", color=Green,  name="Bashrc" },
            [".bash_profile"]  = { icon="", color=Green,  name="Bashrc" },
            [".gitlab-ci.yml"] = { icon="", color=Orange, name="Gitlab" },
            [".gitignore"]     = { icon="", color=Blue,   name="Gitignore" },
         },
      })
   end
}
