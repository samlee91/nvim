
return {
   "akinsho/bufferline.nvim",
   dependencies = { "nvim-tree/nvim-web-devicons" },
   enabled = true,
   lazy = false,

   config = function()
      local status, bufferline = pcall(require, "bufferline")
      if not status then
         return
      end

      bufferline.setup({
         options = {
            mode = "tabs",
            numbers = "none",

            close_icon = false,
            left_truc_marker = false,
            right_truc_marker = false,
            modified_icon = "",

            max_name_length = 18,
            max_prefix_length = 15,
            truncate_names = true,
            tab_size = 25,

            diagnostics = false,
            diagnostics_update_in_insert = false,

            themable = true,
            color_icons = true,

            show_buffer_icons = true,
            show_buffer_close_icons = false,
            show_close_icon = false,
            show_tab_indicators = false,
            show_duplicate_prefix = true,

            separator_style = "slant",
            enforce_regular_tabs = true,
            always_show_bufferline = true,

            hover = {
               enabled = false,
            },
         },

         highlights = {
            fill = { bg=DarkBlack },
            background = { fg=Gray, bg=Black, bold=true, italic=true },
            buffer_selected = { fg=White, bg=Background, bold=true, italic=true },
            separator = { fg=DarkBlack, bg=Black },
            separator_selected = { fg=DarkBlack, bg=Background },
            modified = { fg=Green, bg=Black },
            modified_selected = { fg=Green, bg=Background },
         }
      })
   end

}
