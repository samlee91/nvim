
return {
   "folke/noice.nvim",
   lazy = false,
   enabled = true,

   config = function()
      local status, noice = pcall(require, "noice")
      if not status then
         return
      end

      noice.setup({
         cmdline = {
            enabled = true,
            view = "cmdline_popup",
            format = {
               cmdline     = { icon="   ", title=" Command ", lang="vim",   pattern="^:" },
               search_down = { icon="   ", title=" Search ",  lang="regex", pattern="^/" },
               search_up   = { icon="   ", title=" Search ",  lang="regex", pattern="^%?" },
               filter      = { icon="   ", title=" Shell ",   lang="bash",  pattern="^:%s*!" },
               lua         = { icon="   ", title=" Lua ",     lang="lua",   pattern="^:%s*lua%s+" },
               sed         = { icon=" 󱁴  ",  title=" Sed ",     lang="vim",   pattern="^:%s*%%s", conceal=false },
               help        = { icon=" 󱓷  ",  title=" Help ",    lang="vim",   pattern={ "^:%s*he?l?p?%s+", "^:%s*vert he?l?p?%s+" } },
            },
         },

         messages = {
            enabled = true,
            view = "notify",
            view_error = "popup",
            view_warn = "notify",
            view_search = false,   --> disable inline search count
         },

         notify = {
            enabled = true,
            view = "notify",
         },

         lsp = {
            override = {
               [ "vim.lsp.util.convert_input_to_markdown_lines" ] = true,
               [ "vim.lsp.util.stylize_markdown" ] = true,
               [ "cmp.entry.get_documentation" ] = true,
            },
            hover = { enabled=false },
            signature = { enabled=true },
            progress = { enabled=false }, --> disable "loading workspace" message
         },

         views = {
            cmdline_popup = {
               position = { row="30%", col="50%", },
               size = { width=60, height=1, },
               border = { padding={1, 1} }
            },

            popup = {
               wrap = true,
               position = { row="50%", col="50%", },
               size = { width=60, height=10 },
               border = { padding={1, 1} },
               win_options = {
                  wrap = true,
                  smartindent = true,
               },
            },
         },

         presets = {
            long_message_to_split = true,
            lsp_doc_border = true,
         },

         status = {
            enabled=false,
         },

         routes = {
            { --> disable write command response
               filter = {
                  find = "written",
                  event = "msg_show",
                  kind = "",
               },
               opts = { skip=true },

            },{ --> disable undo/redo messages
               filter = {
                  find = "seconds ago",
                  event = "msg_show"
               },
               opts = { skip=true },

            },{ --> show macro @recording message
               view = "notify",
               filter = { event="msg_showmode", },

            },{ --> LaTeX compile message
               view = "notify",
               filter = {
                  find = "pdflatex",
                  event = "msg_show"
               },
               opts = { skip=true },

            },{ --> disable duplicate search messages
               filter = {
                  find = "Pattern not found",
                  event = "msg_show",
                  kind = "emsg",
               },
               view = "notify",
               opts = { replace=true, merge=true },

            },{ --> disable search count messages
               filter = {
                  event = "msg_show",
                  kind = "search_count"
               },
               opts = { skip=true },

            },{ --> hide search result messages
               filter = {
                  find = "/",
                  event = "msg_show",
                  kind = ""
               },
               opts = { skip=true },
            },
         },

      })
   end,


   dependencies = {
      "MunifTanjim/nui.nvim",
      { "rcarriga/nvim-notify",
         lazy = false,
         enabled = true,

         config = function()
            local status, notify = pcall(require, "notify")
            if not status then
               return
            end

            vim.keymap.set("n", "<leader>dd", notify.dismiss)

            notify.setup({
               fps = 60,
               title = "",

               icons = {
                  DEBUG = " ",
                  ERROR = " ",
                  INFO = " ",
                  TRACE = "✎ ",
                  WARN = " "
               },

               level = 2,
               minimum_width = 50,
               render = "wrapped-compact",
               stages = "slide",
               timeout = 3000,
               top_down = true
            })
         end
      }
   }
}
