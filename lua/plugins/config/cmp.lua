
return {
   "hrsh7th/nvim-cmp",
   enabled = true,
   lazy = false,

   dependencies = {
      "hrsh7th/nvim-cmp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
      "hrsh7th/cmp-nvim-lsp",
      "f3fora/cmp-spell",
      "saadparwaiz1/cmp_luasnip",
      "rafamadriz/friendly-snippets",

      { "L3MON4D3/LuaSnip",
         dependencies = { "friendly-snippets" },
         config = function()
            local status, luasnip = pcall(require, "luasnip")
            if not status then
               return
            end

            luasnip.config.set_config({
               history = false,
               updateevents = "TextChanged,TextChangedI",
            })
         end,
      },
   },

   config = function()
      local status, cmp = pcall(require, "cmp")
      if not status then
         return
      end

      local kind_icons = {
         Text          = " ",
         Function      = " ",
         Method        = " ",
         Keyword       = " ",
         Constructor   = " ",
         Variable      = "α ",
         Field         = "α ",
         Class         = "拾",
         Interface     = " ",
         Snippet       = " ",
         Module        = " ",
         Property      = " ",
         Unit          = " ",
         Enum          = " ",
         Color         = " ",
         File          = " ",
         Reference     = " ",
         Folder        = " ",
         EnumMember    = " ",
         Constant      = " ",
         Struct        = " ",
         Event         = " ",
         Operator      = " ",
         TypeParameter = "",
         Command       = "C",
      }

      local menu_icons = {
         buffer   = " ",
         nvim_lsp = " ",
         cmdline  = " ",
         path     = " ",
         spell    = "󱓷 ",
      }

      cmp.setup {
         preselect = cmp.PreselectMode.None,
         completion = { completeopt="menu,menuone,noinsert" },

         mapping = {
            ["<Tab>"]         = cmp.mapping.select_next_item(),
            ["<S-Tab>"]       = cmp.mapping.select_prev_item(),
            ["<S-Backspace>"] = cmp.mapping.abort(),
            ["<C-b>"]         = cmp.mapping.scroll_docs(-4),
            ["<C-f>"]         = cmp.mapping.scroll_docs(4),
            ["<Return>"]      = cmp.mapping.confirm({ select=true }),
         },

         window = {
            documentation = cmp.config.window.bordered({
               border = "rounded",
               winhighlight = "Normal:@popup.cmp,CursorLine:@popup.select.cmp,FloatBorder:@popup.border.cmp",
               max_width = 10,
            }),
            completion = cmp.config.window.bordered({
               border = "rounded",
               winhighlight = "Normal:@popup.cmp,CursorLine:@popup.select.cmp,FloatBorder:@popup.border.cmp",
               side_padding = 1,
               col_offset = 3,
               scrollbar = false,
            }),
         },

         formatting = {
            fields = { "menu", "abbr", "kind" },
            format = function(entry, item)
               item.kind = string.format("%s", kind_icons[item.kind] or "[]")
               item.menu = string.format("%s", menu_icons[entry.source.name] or "[]")
               return item
            end,
         },

         snippet = {
            expand = function(args)
               local ok, snippet_expand = pcall(require("luasnip").lsp_expand, args.body)
               return ok and snippet_expand or ""
            end
         },

         sources = {
            { name = "buffer" },
            { name = "nvim_lsp" },
            { name = "luasnip" },
            {
               name = "spell",
               option = {
                  keep_all_entries = false,
                  enable_in_context = function()
                     return require('cmp.config.context')
                           .in_treesitter_capture('spell')
                  end,
               }
            },
         },
      }

      cmp.setup.cmdline(":", {
         mapping = cmp.mapping.preset.cmdline(),

         sources = {
            { name = "path" },
            { name = "cmdline" },
         },

         formatting = {
            fields = { "menu", "abbr" },
            format = function(entry, item)
               item.menu = string.format("%s", menu_icons[entry.source.name] or "[]")
               return item
            end,
         },
      })

   end
}

