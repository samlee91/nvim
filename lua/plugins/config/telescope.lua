
return {
   "nvim-telescope/telescope.nvim",
   lazy = false,
   enabled = true,

   dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons",
   },

   config = function()
      local status, telescope = pcall(require, 'telescope')
      if not status then
         return
      end

      local find_files = require("telescope.builtin").find_files
      local opts = { silent=true }
      vim.keymap.set("n", "<leader>ff.", function() find_files() end, opts)
      vim.keymap.set("n", "<leader>ffh", function() find_files({ search_dirs={"$HOME"} }) end, opts)
      vim.keymap.set("n", "<leader>ffc", function() find_files({ search_dirs={"$XDG_CONFIG_HOME"} }) end, opts)
      vim.keymap.set("n", "<leader>ffn", function() find_files({ search_dirs={"$XDG_CONFIG_HOME/nvim"} }) end, opts)

      telescope.setup({
         defaults = {
            prompt_prefix = " 🔍 ",
            entry_prefix = "   ",
            selection_caret = " • ",
            sorting_strategy = "ascending",
            color_devicons = true,

            layout_config = {
               prompt_position = "top",
               width = 0.5,
               height = 0.5,
            },

            path_display = {
               truncate = 3,
               shorten = 1,
            },

            mappings = {
               i = { ["<CR>"] = require("telescope.actions").select_tab },
               n = { ["<CR>"] = require("telescope.actions").select_tab },
            },
         },

         pickers = {
            find_files = {
               preview_title = "",
               prompt_title = "🔭",
               --> prompt_title = " ",
               results_title = false,
               line_padding = 10,
            },
         },

      })
   end
}

