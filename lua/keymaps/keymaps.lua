local utils = require( "keymaps.utils" )
vim.g.mapleader = " "


--> source current file
vim.keymap.set( "n", "<leader>so", ":source % <Return>", { silent=true } )

--> motions
vim.keymap.set( { "n", "v" }, "J", "}" )
vim.keymap.set( { "n", "v" }, "K", "{" )
vim.keymap.set( { "n", "v" }, "<C-J>", "<C-d>" )
vim.keymap.set( { "n", "v" }, "<C-K>", "<C-u>" )

--> tabs
vim.keymap.set(  "n", "<C-o>", ":tabnew "  )
vim.keymap.set( "n", "<C-l>", ":tabnext <Return>", { silent=true } )
vim.keymap.set( "n", "<C-h>", ":tabprev <Return>", { silent=true } )

--> splits
vim.keymap.set( "n", "<leader>ho",  ":split " )
vim.keymap.set( "n", "<leader>vo",  ":vsplit " )
vim.keymap.set( "n", "<leader>hh",  "<C-w>h" )
vim.keymap.set( "n", "<leader>jj",   "<C-w>j" )
vim.keymap.set( "n", "<leader>kk",   "<C-w>k" )
vim.keymap.set( "n", "<leader>ll",   "<C-w>l" )

--> window resizing
vim.keymap.set( "n", "<C-A-j>", ":resize -2 <Return>" )
vim.keymap.set( "n", "<C-A-k>", ":resize +2 <Return>" )
vim.keymap.set( "n", "<C-A-l>", ":vertical resize +2 <Return>" )
vim.keymap.set( "n", "<C-A-h>", ":vertical resize -2 <Return>" )

--> change/delete to underscore
vim.keymap.set( "n", "cu", "ct_" )
vim.keymap.set( "n", "du", "df_" )

--> sed global
vim.keymap.set( "n", "<leader>sg", ":%s/" )

--> yank line segment 
vim.keymap.set( "n", "Y", "y$" )

--> capitalize word
vim.keymap.set( "n", "<leader>~", "ve~" )

--> copy paste from clipboard in wayland
vim.keymap.set(  "v", "<leader>yy", ":w !wl-copy --trim-newline  <Return><Return>", { silent=true } )
vim.keymap.set(  "n", "<leader>pp", ":r !wl-paste <Return>" )

--> align at "="
vim.keymap.set( "v", "<leader>=", "!column -t -s= -o= <Return>" )

--> end of line
vim.keymap.set({ "n", "v" }, "E", "$" )

--> concatenate lines
vim.keymap.set( "n", "<leader>ca", "J" )

--> format indentation
vim.keymap.set( "n", "<leader>fi", "mFgg=G`F" )

--> indent selected block
vim.keymap.set( "v", "<Tab>", ">gv" )
vim.keymap.set( "v", "<S-Tab>", "<gv" )

--> keep centered while searching
--> vim.keymap.set( "n", "n", "nzzzv" )
--> vim.keymap.set( "n", "N", "Nzzzv" )

--> execute macros on selection
vim.keymap.set( "v", "<leader>@", ":normal! @" )

--> open hyprlink in browser
vim.keymap.set( "n", "<leader>hl", "gx" )

--> toggle statusline
vim.keymap.set( "n", "<leader>ss", utils.toggle_statusline )

--> lsp
vim.keymap.set( "n", "<leader>E", vim.diagnostic.open_float, { buffer=bufnr } )
vim.keymap.set( "n", "<leader>K", vim.lsp.buf.hover, { buffer=bufnr } )
--> vim.keymap.set( "i", "<C-p>", function()
-->    vim.cmd.stopinsert()
-->    vim.lsp.buf.signature_help()
-->    vim.defer_fn(function() vim.cmd.wincmd("w") end, 100)
-->    vim.keymap.set("n", "q", ":close<CR>", { buffer = true })
--> end)

--> buffer info in a popup
vim.keymap.set( "n", "<leader>bd", utils.bufdump )



-->   ██████╗███╗   ███╗██████╗ 
-->  ██╔════╝████╗ ████║██╔══██╗
-->  ██║     ██╔████╔██║██║  ██║
-->  ██║     ██║╚██╔╝██║██║  ██║
-->  ╚██████╗██║ ╚═╝ ██║██████╔╝
-->   ╚═════╝╚═╝     ╚═╝╚═════╝ 

--> open help in vertical split
vim.keymap.set( "c", "hh ", "vert h " )

--> pretty print
vim.keymap.set( "c", "PP", "lua vim.print(  )<left><left>" )




-->  ██████╗ ██████╗  █████╗  ██████╗██╗  ██╗███████╗████████╗███████╗
-->  ██╔══██╗██╔══██╗██╔══██╗██╔════╝██║ ██╔╝██╔════╝╚══██╔══╝██╔════╝
-->  ██████╔╝██████╔╝███████║██║     █████╔╝ █████╗     ██║   ███████╗
-->  ██╔══██╗██╔══██╗██╔══██║██║     ██╔═██╗ ██╔══╝     ██║   ╚════██║
-->  ██████╔╝██║  ██║██║  ██║╚██████╗██║  ██╗███████╗   ██║   ███████║
-->  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚══════╝   ╚═╝   ╚══════╝

--> autopairs
vim.keymap.set( "i", "{", "{}<Esc>i" )
vim.keymap.set ( "i", "(", "()<Esc>i")
vim.keymap.set( "i", "[", "[]<Esc>i" )

--> wrap word in brackets
vim.keymap.set( "n", "<leader>(", "i{<ESC>ea}<Esc>%" )
vim.keymap.set( "n", "<leader>(", "i(<ESC>ea)<Esc>%" )
vim.keymap.set( "n", "<leader>[", "i[<ESC>ea]<Esc>%" )

--> autoindent bracket pairs
utils.bracketmap( "i", "<Return>", "<Return><Esc>O" )

 --> double space inside empty brackets
utils.bracketmap( "i", "<Space>", "<Space><Esc>i<Space>" )

--> lazy
vim.keymap.set( "n", "<leader><C-l>", ":Lazy <Return>" )
