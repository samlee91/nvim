local M = {}

function M.popup(width, height, popup_syntax, breakindent_settings, lines)
   local popup_width = width
   local popup_height = height
   local popup_buf = vim.api.nvim_create_buf(false, true)
   local ui = vim.api.nvim_list_uis()[1]

   vim.api.nvim_open_win(popup_buf, true, {
      relative = "editor",
      border = "rounded",
      anchor = "NW",
      width = popup_width,
      height = popup_height,
      row = ( ui.height - popup_height ) / 2,
      col = ( ui.width - popup_width ) / 2,
      zindex = 2,
   })

   vim.wo.number = false
   vim.wo.relativenumber = false
   vim.wo.cursorline = false
   vim.bo.smartindent = true
   vim.wo.wrap = true
   vim.wo.breakindentopt = breakindent_settings
   vim.wo.foldcolumn = "2"
   vim.bo.filetype = popup_syntax

   vim.keymap.set("n", "q",     ":q<Return>", { buffer=true, silent=true })
   vim.keymap.set("n", "<Esc>", ":q<Return>", { buffer=true, silent=true })
   vim.keymap.set("n", "j",     "gj",         { buffer=true, silent=true })
   vim.keymap.set("n", "k",     "gk",         { buffer=true, silent=true })

   for i, line in ipairs(lines) do
      vim.api.nvim_buf_set_lines(popup_buf, i-1, -1, false, { line })
   end
end



function M.bracketmap(mode, keypress, mapping, brackets)
   if brackets == nil then
      brackets = { "{}", "()", "[]" }
   end

   vim.keymap.set(mode, keypress, function()
      local pos = vim.fn.col(".")
      local current_chars = vim.fn.getline("."):sub(pos - 1, pos)

      for _, value in pairs(brackets) do
         if current_chars == value then
            return mapping
         end
      end
      return keypress
   end, { expr=true })
end





function M.toggle_statusline()
   if vim.o.statusline == "" then
      return dofile(vim.fn.stdpath("config") .. "/lua/statusline/statusline.lua")
   end
   vim.o.statusline = ""
end


function M.bufdump()
   local filename = vim.fn.expand("%:p")
   local filetype = vim.bo.filetype
   local filesize = string.format("%.2f", vim.fn.wordcount()["bytes"] / 1000)
   local numlines = vim.fn.line("$")
   local filetype_icon = ""

   local status, devicons = pcall(require, "nvim-web-devicons")
   if status then
      filetype_icon = devicons.get_icon_by_filetype(filetype)
   end

   return M.popup(50, 8, "yaml", "shift:10", {
      "",
      "filename: " .. filename,
      "filetype: " .. filetype .. " " .. filetype_icon,
      "kilobytes: " .. filesize,
      "lines: " .. numlines,
   })
end



return M
