vim.o.termguicolors = true
require("theme.colors")

local load_colors = require("theme.utils").load_colors

load_colors("nvim")
load_colors("treesitter")
load_colors("statusline")
load_colors("telescope")
load_colors("cmp")
load_colors("lsp")
load_colors("notify")
load_colors("noice")
load_colors("lazy")
