local M = {}

M.highlight_directory = "theme.highlight."

function M.load_colors(file)
   local status, nvim_groups = pcall(require, M.highlight_directory .. file)
   if not status then
      vim.print("colorscheme error: " .. file .. " does not exist.")
      return
   end

   for group, colors in pairs(nvim_groups) do
      vim.api.nvim_set_hl(0, group, colors)
   end
end


function M.get_hl( hl_group, hl_attr )
   local ok, attr = pcall( vim.api.nvim_get_hl, 0, { name=hl_group, link=false })

   if ok and attr[hl_attr] ~= nil then
      return string.format("#%06x", attr[hl_attr])
   end
end


function M.link_fg( hl_group )
   return M.get_hl( hl_group, "fg" )
end

function M.link_bg( hl_group )
   return M.get_hl( hl_group, "bg" )
end


return M
