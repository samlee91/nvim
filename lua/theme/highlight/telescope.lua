return {
   ['TelescopeSelection'] = { fg=Blue, italic=true, bold=true },
   ['TelescopeSelectionCaret'] = { fg=Green },
   ['TelescopeNormal'] = { fg=Foreground },
   ['TelescopePreviewNormal'] = { fg='None'},
   ['TelescopeBorder'] = { fg=Gray },
   ['TelescopePromptCounter'] = { fg=Background },
   ['TelescopeMatching'] = { link='TelescopeSelection' },
   ['TelescopePromptPrefix'] = { fg=Purple },
   ['TelescopePromptNormal'] = { fg=Foreground, bold=true },
   --> ['TelescopePromptNormal'] = { fg=Foreground, bold=true, italic=true },
   ['TelescopePromptTitle'] = { fg=Gray, bold=true },

   --> TelescopePromptBorder
   --> TelescopeResultsBorder
   --> TelescopePreviewBorder
   --> TelescopeResultsNormal
   --> TelescopeMultiSelection
   --> ['TelescopeMultiIcon']
   --> ['TelescopeTitle'] = Blue,
   --> TelescopeResultsTitle
   --> TelescopePreviewTitle
   --> TelescopePreviewLine
   --> TelescopePreviewMatch
   --> TelescopePreviewPipe
   --> TelescopePreviewCharDev
   --> TelescopePreviewDirectory
   --> TelescopePreviewBlock
   --> TelescopePreviewLink
   --> TelescopePreviewSocket
   --> TelescopePreviewRead
   --> TelescopePreviewWrite
   --> TelescopePreviewExecute 
   --> TelescopePreviewHyphen 
   --> TelescopePreviewSticky
   --> TelescopePreviewSize 
   --> TelescopePreviewUser
   --> TelescopePreviewGroup
   --> TelescopePreviewDate 
   --> TelescopePreviewMessage 
   --> TelescopePreviewMessageFillchar 
   --> TelescopeResultsClass
   --> TelescopeResultsConstant
   --> TelescopeResultsField 
   --> TelescopeResultsFunction
   --> TelescopeResultsMethod
   --> TelescopeResultsOperator
   --> TelescopeResultsStruct 
   --> TelescopeResultsVariable
   --> TelescopeResultsLineNr 
   --> TelescopeResultsIdentifier 
   --> TelescopeResultsNumber 
   --> TelescopeResultsComment
   --> TelescopeResultsSpecialComment 
   --> TelescopeResultsDiffChange 
   --> TelescopeResultsDiffAdd 
   --> TelescopeResultsDiffDelete 
   --> TelescopeResultsDiffUntracked 
}
