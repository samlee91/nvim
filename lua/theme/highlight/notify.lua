return {
   ["NotifyBackground"] = { bg="#000000" },
   ["NotifyERRORBorder"] = { fg=DarkBlack },
   ["NotifyWARNBorder"] = { link="NotifyERRORBorder" },
   ["NotifyINFOBorder"] = { link="NotifyERRORBorder" },
   ["NotifyDEBUGBorder"] = { link="NotifyERRORBorder" },
   ["NotifyTRACEBorder"] = { link="NotifyERRORBorder" },

   ["NotifyERRORIcon"] = { fg=Red },
   ["NotifyWARNIcon"] = { fg=Orange },
   ["NotifyINFOIcon"] = { fg=Blue },
   ["NotifyDEBUGIcon"] = { fg=Green },
   ["NotifyTRACEIcon"] = { fg=Yellow },

   ["NotifyERRORTitle"] = { link="NotifyERRORIcon" },
   ["NotifyWARNTitle"] = { link="NotifyWARNIcon" },
   ["NotifyINFOTitle"] = { link="NotifyINFOIcon" },
   ["NotifyDEBUGTitle"] = { link="NotifyDEBUGIcon" },
   ["NotifyTRACETitle"] = { link="NotifyTRACEIcon" },

   ["NotifyERRORBody"] = { fg=Foreground },
   ["NotifyWARNBody"] = { link="NotifyERRORBody" },
   ["NotifyINFOBody"] = { link="NotifyERRORBody" },
   ["NotifyDEBUGBody"] = { link="NotifyERRORBody" },
   ["NotifyTRACEBody"] = { link="NotifyERRORBody" },
}
