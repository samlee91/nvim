return {
   ["BufferLineCloseButton"] = { fg=DarkGray, bg=DarkGray },
   ["BufferLineFill"] = { fg=DarkGray, bg=DarkBlack }, --> background
   ["BufferLineBufferSelected"] = { fg=White, bold=true, italic=true }, --> active tab
   ["BufferLineBackground"] = { fg=Gray, bg=Black, bold=true, italic=true }, --> inactive tab
   ["BufferLineSeparator"] = { fg=DarkBlack, bg=Black }, --> inactive separator
   ["BufferLineSeparatorSelected"] = { fg=DarkBlack }, --> active separator
   ["BufferLineModified"] = { fg=Green, bg=Black },
   ["BufferLineModifiedSelected"] = { fg=Green },
   ["BufferLineBuffer"] = { fg=Black, bg=DarkGray } ,

   --> ["BufferLineHint"]
   --> ["BufferLineHintVisible"]
   --> ["BufferLineHintSelected"]
   --> ["BufferLineWarning"]
   --> ["BufferLineWarningVisible"]
   --> ["BufferLineWarningSelected"]
   --> ["BufferLineWarningDiagnostic"]
   --> ["BufferLineWarningDiagnosticVisible"]
   --> ["BufferLineWarningDiagnosticSelected"]
   --> ["BufferLineDuplicateVisible"]
   --> ["BufferLineDuplicate"]
   --> ["BufferLineDuplicateSelected"]
   --> ["BufferLineDiagnostic"]
   --> ["BufferLineDiagnosticSelected"]
   --> ["BufferLineDiagnosticVisible"]
   --> ["BufferLineInfo"]
   --> ["BufferLineInfoVisible"]
   --> ["BufferLineInfoSelected"]
   --> ["BufferLineInfoDiagnostic"]
   --> ["BufferLineInfoDiagnosticVisible"]
   --> ["BufferLineInfoDiagnosticSelected"]
   --> ["BufferLineIndicatorVisible"]
   --> ["BufferLineIndicatorSelected"]
   --> ["BufferLineHintDiagnostic"]
   --> ["BufferLineHintDiagnosticVisible"]
   --> ["BufferLineHintDiagnosticSelected"]
   --> ["BufferLineError"]
   --> ["BufferLineErrorVisible"]
   --> ["BufferLineErrorDiagnostic"]
   --> ["BufferLineErrorDiagnosticVisible"]
   --> ["BufferLineErrorDiagnosticSelected"]
   --> ["BufferLineErrorSelected"]
   --> ["BufferLineSeparatorVisible"]
   --> ["BufferLineOffsetSeparator"]
   --> ["BufferLineGroupSeparator"]
   --> ["BufferLineGroupLabel"]
   --> ["BufferLineCloseButtonVisible"]
   --> ["BufferLineCloseButtonSelected"]
   --> ["BufferLineBufferVisible"]
   --> ["BufferLineTab"]
   --> ["BufferLineTabSelected"]
   --> ["BufferLineTabSeparator"]
   --> ["BufferLineTabClose"]
   --> ["BufferLineTabSeparatorSelected"]
   --> ["BufferLinePick"]
   --> ["BufferLinePickSelected"]
   --> ["BufferLinePickVisible"]
   --> ["BufferLineNumbers"]
   --> ["BufferLineNumbersSelected"]
   --> ["BufferLineNumbersVisible"]
}
