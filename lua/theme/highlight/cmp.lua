
return {
   --> treesitter
   ["@popup.cmp"] = { fg=Foreground },
   ["@popup.select.cmp"] = { fg=Background, bg=Blue },
   ["@popup.border.cmp"] = { fg=Gray },
   ["@popup.cmd.cmp"] = { fg=White, bg=Gray },

   --> syntax
   ["CmpItemAbbr"] = { fg=Gray },
   ["CmpItemAbbrMatch"] = { fg=Purple, bold=true },
   ["CmpItemAbbrMatchFuzzy"] =  { fg=Cyan, bold=true },
   ["CmpItemKind"] = { fg=Yellow,  bold=true },
   ["CmpItemMenu"] = { fg=Green },

   ["CmpItemKindCommand"] = { fg=Cyan, bold=true },
   ["CmpItemMenucmdline"] = { fg=Cyan, bold=true },

   --> highlight each menu/kind item using 
   --> "CmpItemKind<item_name>"
   --> "CmpItemMenu<item_name>"
}
