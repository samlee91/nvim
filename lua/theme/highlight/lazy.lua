
return {

   ["LazyButton"] = { fg=Purple, bold=true, italic=true },
   ["LazyButtonActive"] = { fg=Blue, bold=true, italic=true },
   ["LazyH1"] = { fg=Blue, bold=true, italic=true },
   ["LazyH2"] = { fg=Cyan },
   ["LazyNormal"] = { fg=Foreground },
   ["LazyComment"] = { link="@comment" },

   --> expanded item keys
   ["LazyProp"] = { fg=Purple },

   --> bullets and keypress letters for actions "(*)"
   ["LazySpecial"] = { fg=Yellow, bold=true },

   ["LazyReasonStart"] = { fg=Blue },
   ["LazyReasonSource"] = { link="LazyReasonStart" },
   ["LazyReasonCmd"] = { link="LazyReasonStart" },
   ["LazyReasonEvent"] = { link="LazyReasonStart" },
   ["LazyReasonFt"] = { link="LazyReasonStart" },
   ["LazyReasonImport"] = { link="LazyReasonStart" },
   ["LazyReasonKeys"] = { link="LazyReasonStart" },
   ["LazyReasonPlugin"] = { link="LazyReasonStart" },
   ["LazyReasonRuntime"] = { link="LazyReasonStart" },

   ["LazyCommit"] = { link="LazyH2" },
   ["LazyCommitIssue"] = { link="LazyCommit" },
   ["LazyCommitType"] = { fg=Blue },
   ["LazyCommitScope"] = { link="LazyCommit" },

   ["LazyDimmed"] = { link="LazyComment" },
   --> ["LazyDir"] = { fg=Green },
   --> ["LazyLocal"] = { fg=Green },
   --> ["LazyNoCond"] = { fg=Green },
   --> ["LazyProgressDone"] = { fg=Green },
   --> ["LazyProgressTodo"] = { fg=Green },
   --> ["LazyTaskError"] = { fg=Green },
   --> ["LazyTaskOutput"] = { fg=Green },
   --> ["LazyUrl"] = { fg=Green },
   --> ["LazyValue"] = { fg=Green },
}
