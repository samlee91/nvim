
return {
   --> main
	["Character"] = { fg=Green },
	["Comment"] = { fg=Gray, bold=true, italic=true },
	["Constant"] = { fg=Cyan },
	["Conditional"] = { fg=Purple },
	["String"] = { fg=Green },
	["Statement"] = { fg=Purple },
	["Function"] = { fg=Blue },
	["Special"] = { fg=Blue },
	["Repeat"] = { fg=Purple },
	["Operator"] = { fg=Purple },
	["Keyword"] = { fg=Purple },
	["Label"] = { fg=Purple },
	["Exception"] = { fg=Purple },
	["Include"] = { fg=Purple },
	["Define"] = { fg=Purple },
	["Precondit"] = { fg=Purple },
	["StorageClass"] = { fg=Purple },
   ["None"] = { fg=Foreground },
	["Normal"] = { fg="None" },
	["NormalFloat"] = { bg=Background },
	["Number"] = { fg=Orange },
	["Boolean"] = { fg=Orange },
	["Float"] = { fg=Orange },
	["SpecialComment"] = { fg=Orange, italic=true },
	["Preproc"] = { fg=Yellow },
	["Type"] = { fg=Yellow },
	["Tag"] = { fg=Yellow },
	["Identifier"] = { fg=Foreground },
	["Macro"] = { fg=Cyan },
	["Structure"] = { fg=Cyan },
	["TypeDef"] = { fg=Cyan },
	["SpecialChar"] = { fg=Cyan },
	["Underlined"] = { fg=Cyan },
	["Debug"] = { fg=Yellow },
	["Error"] = { fg=Red },
	["ErrorMsg"] = { fg=Foreground },
	["ALEError"] = { fg=Red },
	["ALEErrorLine"] = { fg=Red },
	["Ignore"] = { fg=DarkGray },
	["Delimiter"] = { fg=Cyan },
	["Title"] = { fg=Blue, bold=true },
	["Search"] = { reverse=true, bold=true, italic=true },

   --> editor
	["SpellBad"] = { sp=Red, undercurl=true },
	["MatchParen"] = { reverse=true, bold=true },
	["CursorLine"] = { bg=DarkGray },
	["CursorLineNr"] = { fg=White, bg=DarkGray, bold=true },
	["Folded"] = { fg=Gray, italic=true },
	["LineNr"] = { fg=Gray },
	["FoldColumn"] = { fg=Background },
	["SignColumn"] = { fg=Background },
	["EndOfBuffer"] = { fg=Background },
   ["WinSeparator"] = { fg=Gray },
	["VertSplit"] = { link="Comment" },
	["Visual"] = { reverse=true },

   --> mini.animate
   ["MiniAnimateCursor"] = { bg=White },
	["MiniAnimateNormalFloat"] = { fg=Background, bg=Background },


   --> lines
	["StatusLine"] = { fg=Background },
	["StatusLineNC"] = { fg=Background, bold=true },
   ["WinBar"] = { fg=Background, bg=Background }, --> blank line under tabline for spacing
   ["WinBarNC"] = { link="WinBar" }, --> blank line under tabline for spacing

   --> cmdline
   ["MsgArea"] = { fg=Foreground, bg=DarkBlack },

   --> diagnostics
	["WarningMsg"] = { link="Error" },
	["DiagnosticError"] = { link="Error" },
	["DiagnosticWarn"] = { fg=Yellow },
	["DiagnosticInfo"] = { fg=Blue },
	["DiagnosticHint"] = { fg=Green },
	["DiagnosticSignError"] = { link="Error" },
	["DiagnosticSignWarn"] = { link="DiagnosticWarn" },
	["DiagnosticSignInfo"] = { link="DiagnosticInfo" },
	["DiagnosticSignHint"] = { link="DiagnosticHint" },
	["DiagnosticFloatingError"] = { link="DiagnosticError" },
	["DiagnosticFloatingWarn"] = { link="DiagnosticWarn" },
	["DiagnosticFloatingInfo"] = { link="DiagnosticInfo" },
	["DiagnosticFloatingHint"] = { link="DiagnosticHint" },

   ["DiagnosticVirtualTextHint"] = { link="DiagnosticHint" },
	["DiagnosticUnderlineError"] = { underline=true, sp=Red },
	["DiagnosticUnderlineWarn"] = { underline=true, sp=Yellow },
	["DiagnosticUnderlineInfo"] = { underline=true, sp=Blue },
	["DiagnosticUnderlineHint"] = { underline=true, sp=Green },
   ["DiagnosticUnnecessary"] = { underline=true, sp=Orange },

   --> netrw
   ["netrwDir"] = { fg=Blue, bold=true },
   ["netrwClassify"] = { fg=Cyan, bold=true },

   --> popup menu
	["Pmenu"] = { fg=Foreground },
	["PmenuSel"] = { fg=Background, bg=Blue },
	["PmenuSbar"] = { fg=Gray, bg=Gray },
	["PmenuThumb"] = { fg=Gray, bg=Gray },
	["FloatBorder"] = { fg=Gray },

   --> languages
	["dosiniLabel"] = { fg=Blue },
	["dosiniHeader"] = { fg=Purple },

   --> lisp
   ["lispList"] = { fg=Blue },
   ["lispSymbol"] = { fg=Blue },
   ["lispParen"] = { fg=Purple },

   --> *sh
   ["shFunctionOne"] = { fg=Blue },
	["zshMathSubst"] = { fg=Purple },
	["zshSubstDelim"] = { link="Function" },
	["zshPreProc"] = { link="Comment" },
	["zshTypes"] = { fg=Purple },
	["zshTodo"] = { fg=Red, bold=true, italic=true },
	["zshOperator"] = { link="Operator" },
	["zshKSHFunction"] = { link="Function" },
	["zshVariableDef"] = { link="Macro" },

   --> packer
   ["packerSuccess"] = { fg=Green, bold=true },
   ["packerStatusSuccess"] = { fg=Cyan, bold=true, italic=true },
   ["packerFail"] = { link="Error" },

   --> help
   ["helpSectionDelim"] = { link="Title" },
   ["helpCommand"] = { link="Keyword" },

   --> health
   ["healthSuccess"] = { fg=Green, bold=true },
   ["healthHeadingChar"] = { link="Title" },

   --> LaTeX
   ["texStatement"] = { fg=Purple },
   ["texBeginEnd"] = { link="Conditional" },
   ["texBeginEndName"] = { fg=Cyan, italic=true, bold=true },
   ["texDelimeter"] = { link="Conditional" },
   ["texAccent"] = { link="Operator" },
   ["pandocLaTeXMathBlock"] = { link="texMathZoneY" },
   ["pandocLaTeXRegion"] = { link="texMathZoneY" },
}
