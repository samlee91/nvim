return {
   --> syntax
	["LspInfoTitle"] = { fg=Blue, bold=true },
	["LspInfoList"] = { fg=Green },
	["LspInfoFiletype"] = { fg=Purple },
	["LspInfoTip"] = { fg=Cyan },
	["LspInfoBorder"] = { fg=Green },
	["LspReferenceText"] = { fg=Green },
	["LspReferenceRead"] = { fg=Blue },
	["LspReferenceWrite"] = { fg=Purple },
}
