local statusline_tab = Black
local statusline_tab_shadow = DarkBlack


return {
   ["StatusLineTab"] = { fg=statusline_tab, bg=statusline_tab },
   ["StatusLineLeftSeparator"] = { fg=statusline_tab_shadow, bg=statusline_tab },
   ["StatusLineRightSeparator"] = { fg=statusline_tab_shadow, bg=statusline_tab },
   ["StatusLineOutsideLeftSeparator"] = { fg=statusline_tab_shadow, bg=Background },
   ["StatusLineOutsideRightSeparator"] = { fg=statusline_tab_shadow, bg=Background },

   ["@statusline.tab"] = { fg=statusline_tab, bg=statusline_tab },
   ["@statusline.text"] = { fg=White, bg=statusline_tab },

   ["@statusline.outside_separator"] = { fg=statusline_tab_shadow, bg=Background },
   ["@statusline.inside_separator"] = { fg=statusline_tab_shadow, bg=statusline_tab },


   ["@statusline.mode.text"] = { link="@statusline.text" },
   ["@statusline.mode.icon.normal"] = { fg=Blue, bg=statusline_tab, bold=true },

   ["@statusline.mode.icon.insert"] = { fg=Green, bg=statusline_tab, bold=true },
   ["@statusline.mode.icon.command"] = { fg=Cyan, bg=statusline_tab, bold=true },
   ["@statusline.mode.icon.visual"] = { fg=Purple, bg=statusline_tab, bold=true },
   ["@statusline.mode.icon.visual_line"] = { link="@statusline.mode.icon.visual" },
   ["@statusline.mode.icon.visual_line.2"] = { link="@statusline.text" },
   ["@statusline.mode.icon.visual_block"] = { link="@statusline.mode.icon.visual" },
   ["@statusline.mode.icon.visual_block.2"] = { link="@statusline.text" },

   ["@statusline.cursor.text"] = { link="@statusline.mode.text" },
   ["@statusline.cursor.icon"] = { fg=Yellow, bg=statusline_tab, bold=true },

   ["@statusline.percent.text"] = { link="@statusline.mode.text" },
   ["@statusline.percent.icon"] = { fg=Green, bg=statusline_tab, bold=true },
   ["@statusline.percent.icon.1"] = { fg=Cyan, bg=statusline_tab, bold=true },
   ["@statusline.percent.icon.2"] = { fg=Purple, bg=statusline_tab, bold=true },

   ["@statusline.filesize.text"] = { link="@mode.text" },
   ["@statusline.filesize.icon"] = { fg=Cyan, bg=statusline_tab, bold=true },

}
