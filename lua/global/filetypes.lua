
vim.filetype.add({
   extension = {
      h = "c",
   },
   pattern = {
      [".*.rasi"] = "rasi",
      [".*dunstrc"] = "dosini",
      [".*index.theme"] = "dosini",
      [".*git/config"] = "dosini",
      [".*grimpy.conf"] = "dosini",
      [".*imv/config"] = "dosini",
      [".*zathurarc"] = "zathurarc",
      [".*sxhkdrc"] = "sxhkdrc",
      [".*.config/hypr/.*.conf"] = "hyprlang",
   }
})

