
--> General 
vim.opt.errorbells = false
vim.opt.swapfile = false
vim.opt.wrap = false

--> Layout
vim.opt.shiftwidth = 3
vim.opt.tabstop = 3
vim.opt.softtabstop = 3
vim.opt.scrolloff = 10
vim.opt.smartindent = true
vim.opt.expandtab = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.splitright = true
vim.opt.foldcolumn = "1"

--> Formatting
vim.opt.autoindent = true
vim.opt.linebreak = true
vim.wo.signcolumn = "number"
vim.opt.winbar = "blank line"
vim.api.nvim_create_autocmd("FileType", { --> disable automatic comment char on newline
   group = vim.api.nvim_create_augroup("FormatOptions", { clear=true }),
   command = 'lua vim.opt.formatoptions = "jlcqn"'
})

--> Searching
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true

--> Misc
vim.opt.cmdheight = 0
vim.opt.filetype = "on"
vim.opt.spellfile = vim.fn.stdpath("config") .. "/lua/global/spell/en.utf-8.add"
vim.opt.guicursor = "a:block"
vim.opt.mouse = ""
